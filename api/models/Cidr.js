
var Promise = require('promise');


var findCidrs = function(routerId, ip){
  return new Promise(function(resolve, reject){
    // console.log('findCidrs', { routerId: routerId, ip: ip } );
    return Cidr.findOne( { routerId: routerId, ip: ip } )
      .then( function(item){
        if(item){
          // console.log('findCidrs', item.cidr);
          return resolve(item.cidr.split( ',' ));  
        }else{
          return resolve();  
        }
        
      } );
    // Cidr.findOne( { routerId: routerId, ip: ip } ).exec(function(err, item){
    //   if (err) return reject(err);
    //   console.log('Cidr', item);
    //   console.log('findCidrs', item.cidr);
    //   return resolve(item.cidr);
    // });
  });
}

module.exports = {
  schema: true,

  attributes: {
    routerId: {
      type: 'integer',
      required: true,
    },

    ip: {
      type: 'string',
      required: true,
    },

    cidr: {
      type: 'string',
      required: true,
    },

    toJSON: function () {
      var obj = this.toObject();
      obj.cidr = obj.cidr.split(',');
      return obj;
    }
  },

  findCidrs: findCidrs

};
