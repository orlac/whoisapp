var Promise = require('promise');
var async = require('async');
var _ = require('lodash');




describe.only('CirdModel.test', function() {

  describe('insert', function() {
    it('should check insert data', function (done) {

      Cidr.create({
            routerId: 1,
            ip: '1.1.1.1',
            cidr: '173.252.64.0/18,173.252.64.0/19'
        })
          .then( function(){ done(); }  )
          .catch( done );

    });

  });

  describe('find', function() {
    

    it('should check get data', function (done) {

      var next = function(){
        Cidr.findCidrs(1, '1.1.1.1')
        .then(function(masks){
          if(!masks){
            return done('error: mask not find')
          }
          if( !_.isArray(masks)  ){
            return done('error: mask is not array')  
          }
          if( masks.length == 0  ){
            return done('error: mask is empty array')  
          }
          done();
        })
        .catch(done)  
      }

      Cidr.create({
            routerId: 1,
            ip: '1.1.1.1',
            cidr: '173.252.64.0/18,173.252.64.0/19'
        }).then( next )
          .catch( done );
    });
  });

});