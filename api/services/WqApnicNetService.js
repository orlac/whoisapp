'use strict';

var Nightmare = require('nightmare');
var rootUrl = 'http://wq.apnic.net/whois-search/static/search.html';
var _ = require('lodash');
var Promise = require('promise');


var _searchMask = function(){
	
	var _route = jQuery("#query-results td:contains('route:')").map(function(el){
		return  jQuery(this).next('td').text() ;	
	}).get();
	var _CIDR = jQuery("#query-results td:contains('CIDR')").map(function(el){
		return jQuery(this).next('td').text();	
	}).get();
	var res = ([_route, _CIDR]).join(',').split(',');
	return res;
}


var search = function(ip){
	
	return new Promise(function(resolve, reject){
		var ng = new Nightmare({timeout: 1000, loadImages: false, weak: true})
		// rootUrl = process.cwd()+'/test.html';
		console.log('ip', ip);
		ng.goto(rootUrl)
			.type('#searchtext', ip)
			.screenshot(process.cwd()+'/logs/1w.jpg')
			.click('[name="do_search"]')
			.wait('#query-results div')
			.screenshot(process.cwd()+'/logs/2w.jpg')
			.wait(500)
			.screenshot(process.cwd()+'/logs/3w.jpg')
			// .inject('js', jQuery)
			.evaluate(_searchMask, function (mask) {
		        	WhoIshelper.filterMask(mask).then(resolve).catch(reject);
		     	}) // end evaluate
		  	.run(function (err, nightmare)
			{
		        if (err) return console.log('Ошибка', err);
		        console.log( rootUrl, ' stop');
	      	});	
	});
}

var test = function(ip){
	return new Promise(function(resolve, reject){
		var ng = new Nightmare({timeout: 1000, loadImages: false, weak: true})
		rootUrl = process.cwd()+'/test/wq.apnic.net.html';
		ng.goto(rootUrl)
			.screenshot(process.cwd()+'/logs/1w.jpg')
			.screenshot(process.cwd()+'/logs/2w.jpg')
			.inject('js', process.cwd()+'/bower_components/jquery/dist/jquery.min.js')
			.evaluate(_searchMask, function (mask) {
		        	WhoIshelper.filterMask(mask).then(resolve).catch(reject);
		     	}) // end evaluate
		  	.run(function (err, nightmare)
			{
		        if (err) return console.log('Ошибка', err);
		        console.log( rootUrl, ' stop');
	      	});	
	});
}

module.exports = {
	search: search,
	test: test
}