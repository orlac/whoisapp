var Promise = require('promise');
var async = require('async');
var _ = require('lodash');



describe.only('Cird.test', function() {

  describe('parse ping eu', function() {
    it('should check function in service', function (done) {
      PingEuService.test('1.1.1.1').then(function(masks){
          if(!masks){
            return done('error: mask not find')
          }
          if( !_.isArray(masks)  ){
            return done('error: mask is not array')  
          }
          if( masks.length == 0  ){
            return done('error: mask is empty array')  
          }
          done();
        })
        .catch(done);
    });
  });

  describe('parse apnic net', function() {
    it('should check function in service', function (done) {
      WqApnicNetService.test('1.1.1.1').then(function(masks){
          if(!masks){
            return done('error: mask not find')
          }
          if( !_.isArray(masks)  ){
            return done('error: mask is not array')  
          }
          if( masks.length == 0  ){
            return done('error: mask is empty array')  
          }
          done();
        })
        .catch(done);
    });
  });

  describe('parse in controller', function() {
    it('should check function in controller', function (done) {

      PingEuService.search = PingEuService.test;
      WqApnicNetService.search = WqApnicNetService.test;

      var _fns = []
      _fns.push(function(callback){
        var testFn = CidrServiceHelper.getAsyncFn('1.1.1.1', 1);        
        testFn(function(err, masks, isNewMask){
          
          if(err) return done(err);
          if(masks.length == 0) return done('ничего не найдено');
          console.log('masks', masks);
          if(!isNewMask) return done('не запишится в бд');

          callback();
        })
      });

      _fns.push(function(callback){
        var testFn = CidrServiceHelper.getAsyncFn('1.1.1.1', 1);        
        testFn(function(err, masks, isNewMask){
          
          if(err) return done(err);
          if(masks.length == 0) return done('ничего не найдено');
          console.log('masks', masks);
          if(isNewMask) return done('не записалось в бд');

          callback();
        })
      });

      async.waterfall(_fns, done);

    });
  });

});