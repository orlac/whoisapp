'use strict';

var Promise = require('promise');
var async = require('async');
var _ = require('lodash');

module.exports = {


	getAsyncFn : function(_ip, routerId){
		var __fns = []
    	// 1 поиск в бд
    	__fns.push ( 
    		function(callback){
    			
    			// mock it
    			Cidr.findCidrs(routerId, _ip)
	    			.then( function(masks){  
	    				// console.log('masks', masks);
	    				callback(null, masks) 
	    			} )
	    			.catch( callback );
	    	});
    	// 2 поиск в сервисе
    	__fns.push ( 
    		function( masks, callback){
    			
    			if(masks){ 
    				// false - не надо записывать в бд
					return callback(null, masks, false);
    			}
    			async.parallel([

    				function(cb){
    					// mock it
		    			PingEuService.search(_ip).then(function(masks){
		    					return cb(null, masks);
					    	})
		    				.catch( cb );
    				},

    				function(cb){
    					// mock it
		    			WqApnicNetService.search(_ip).then(function(masks){
		    					return cb(null, masks);
					    	})
		    				.catch( cb );
    				},

    				], function(err, masks){

    					// console.log('--masks', masks);

    					if(err) return callback(err);
    					masks = masks.join(',').split(',');
    					masks = WhoIshelper.filterEmptyMask(masks);
    					masks = WhoIshelper.filterIsMask(masks);
    					return callback(null, masks, true);

    			});
    			
	    	});
    	// 3 запись в бд
    	__fns.push ( 
    		function( masks, canInsert, callback){
    			console.log('canInsert', masks);
    			if(!canInsert){
    				return callback(null, masks, canInsert);	
    			}
    			//mock it
    			if(masks.length > 0){
    				Cidr.create({
					  	routerId: routerId,
					  	ip: _ip,
					  	cidr: masks.join(',')
					})
						.then( function(){
							callback(null, masks, canInsert);
						} )
						.catch(callback);
    			}else{
    				callback(null, masks, canInsert);
    			}
    			
	    	});

    	return function(callback){
    		async.waterfall(__fns, function(err, masks, isNewMask){
    			// console.log('masks', err, masks, isNewMask);
    			return callback(err, masks, isNewMask);
    		});
    	}
	}

	// findCidrs: function(routerId, _ip){
	// 	return new Promise(function(resolve, reject){
	// 		return Cidr.findCidrs(routerId, _ip);
	// 	});
		
	// }

	// findCidrs: function(masks, canInsert, callback){
	// 	if(!canInsert){
	// 		return callback(null, masks);	
	// 	}
	// 	//fixture it
	// 	Cidr.create({
	// 	  	routerId: routerId,
	// 	  	ip: _ip,
	// 	  	cidr: masks.join(',')
	// 	})
	// 		.then( function(){
	// 			callback(null, masks);
	// 		} )
	// 		.catch(callback);
		
	// }
}