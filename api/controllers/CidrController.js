var _ = require('lodash');
var async = require('async');

module.exports = {
  /**
   * Useful when need to check if it's server is down or it some logic is broken
   */
  index: function (req, res) {
    // res.ok(null, null, 'HTTP server is working');
    // console.log(req);
    // console.log(req.param());
    // res.send(200, {message: 'Ok.', data: offers} );
    
    var routerId = req.param('id');
    var host = req.param('host');
    var ip = req.param('ip') || '';
    ip = _.uniq( ip.split(',') );
    // var ip = '87.240.143.241'


    // WqApnicNet.search(ip).then(function(mask){
    //         var data = {
    //             host: host,
    //             ip: [ip],
    //             subnet: {
    //                 length: mask.length,
    //                 cidr : mask
    //             } 
    //         };
    //         res.ok(data);       
    //     }).catch(res.serverError);
    //  return;

    var _fns = _.map(ip, function(_ip){
    	return CidrServiceHelper.getAsyncFn(_ip, routerId);
    } 
	// function(_ip){

 //    	var __fns = []
 //    	// 1 поиск в бд
 //    	__fns.push ( 
 //    		function(callback){
 //    			// mock it
 //    			Cidr.findCidrs(routerId, _ip)
	//     			.then( function(masks){ callback(null, masks) } )
	//     			.catch( callback );
	//     	});
 //    	// 2 поиск в сервисе
 //    	__fns.push ( 
 //    		function( masks, callback){
 //    			if(masks){ 
 //    				// false - не надо записывать в бд
	// 				return callback(null, masks, false);
 //    			}
 //    			// mock it
 //    			PingEuService.search(_ip).then(function(masks){
 //    					return callback(null, masks, true);
	// 		    	})
 //    				.catch( callback );
	//     	});
 //    	// 3 запись в бд
 //    	__fns.push ( 
 //    		function( masks, canInsert, callback){
 //    			if(!canInsert){
 //    				return callback(null, masks, canInsert);	
 //    			}
 //    			//fixture it
 //    			Cidr.create({
	// 			  	routerId: routerId,
	// 			  	ip: _ip,
	// 			  	cidr: masks.join(',')
	// 			})
	// 				.then( function(){
	// 					callback(null, masks, canInsert);
	// 				} )
	// 				.catch(callback);
	//     	});

 //    	return function(callback){
 //    		async.waterfall(__fns, function(err, masks, isNewMask){
 //    			return callback(err, masks, isNewMask);
 //    		});
 //    	}
 //    }
    );

    //TODO несколько ip через ,
    //TODO записать в базу
    
    async.parallel(_fns, function(err, masksData){
    	if(err){
    		return res.serverError(err);
    	}
        masksData = _.map(masksData, function(_maskData){
            return _.filter(_maskData, _.isArray);
        });
        // masksData = masksData.shift();
        console.log('masksData', masksData);
    	masksData = masksData.join(',').split(',');
        masksData = WhoIshelper.filterEmptyMask(masksData);
        masksData = WhoIshelper.filterIsMask(masksData);
        console.log('masksData', masksData);
    	masksData = _.uniq(masksData);
    	var data = {
    		host: host,
    		ip: ip,
    		subnet: {
    			length: masksData.length,
				cidr : masksData
    		} 
    	};
    	res.ok(data);  

    })

    /*PingEuService.search(ip).then(function(mask){
    	var data = {
    		host: host,
    		ip: [ip],
    		subnet: {
    			length: mask.length,
				cidr : mask
    		} 
    	};
		res.ok(data);    	
    }).catch(res.serverError);*/

  }
};