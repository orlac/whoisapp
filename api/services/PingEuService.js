'use strict';

var Nightmare = require('nightmare');
var rootUrl = 'http://ping.eu/ns-whois';
var _ = require('lodash');
var Promise = require('promise');

var _searchMask = function(){
	
	var _route = jQuery("#results td:contains('route')").map(function(el){
		
		return  jQuery(this).next('td').text() ;	
	}).get();
	var _CIDR = jQuery("#results td:contains('CIDR')").map(function(el){
		
		return jQuery(this).next('td').text();	
	}).get();
	var res = ([_route, _CIDR]).join(',').split(',');
	return res;
}




var search = function(ip){
	
	return new Promise(function(resolve, reject){
		var ng = new Nightmare({timeout: 1000, loadImages: false, weak: true})
		// rootUrl = process.cwd()+'/test.html';
		ng.goto(rootUrl)
			.type('[name="host"]', ip)
			.screenshot(process.cwd()+'/logs/1.jpg')
			.click('[type="submit"]')
			.wait()
			.screenshot(process.cwd()+'/logs/2.jpg')
			.inject('js', process.cwd()+'/bower_components/jquery/dist/jquery.min.js')
			// .inject('js', jQuery)
			.evaluate(_searchMask, function (mask) {
		        	WhoIshelper.filterMask(mask).then(resolve).catch(reject);
		     	}) // end evaluate
		  	.run(function (err, nightmare)
			{
		        if (err) return console.log('Ошибка', err);
		        console.log( rootUrl, ' stop');
	      	});	
	});
}

var test = function(ip){
	return new Promise(function(resolve, reject){
		var ng = new Nightmare({timeout: 1000, loadImages: false, weak: true})
		rootUrl = process.cwd()+'/test/ping-eu-test.html';
		ng.goto(rootUrl)
			.screenshot(process.cwd()+'/logs/1.jpg')
			.inject('js', process.cwd()+'/bower_components/jquery/dist/jquery.min.js')
			.screenshot(process.cwd()+'/logs/2.jpg')
			.evaluate(_searchMask, function (mask) {
		        	WhoIshelper.filterMask(mask).then(resolve).catch(reject);
		     	}) // end evaluate
		  	.run(function (err, nightmare)
			{
		        if (err) return console.log('Ошибка', err);
		        console.log( rootUrl, ' stop');
	      	});	
	});
}

module.exports = {
	search: search,
	test: test
}