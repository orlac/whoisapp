'use strict';

var Nightmare = require('nightmare');
var rootUrl = 'http://wq.apnic.net/whois-search/static/search.html';
var _ = require('lodash');
var Promise = require('promise');


var filterMask = function(arr){
	return new Promise(function(resolve, reject){
		
		var evens = _.map(arr, function(item){ 
			item = item.trim();
			if(item){
				return item; 
			}
		});
		evens = filterEmptyMask(evens);

		evens = _.uniq(evens);
		console.log( 'mask', evens);
		resolve(evens);
	});
}

var filterEmptyMask = function(arr){
	return _.filter(arr, function(item){ 
		return !!item; 
	});
}

var filterIsMask = function(arr){
	var reg = new RegExp('[A-z]')
	return _.filter(arr, function(item){ 
		return !reg.test(item); 
	});
}


module.exports = {
	filterMask: filterMask,
	filterEmptyMask: filterEmptyMask,
	filterIsMask: filterIsMask,
}